PATCHES=$(shell ls res/patches/*.pd)
PATCHES_WASM=$(subst res/patches,bin,$(PATCHES:.pd=.wasm))
LIBS=$(subst libs,bin,$(shell ls libs/*))

bin:
	mkdir bin

bin/%.wasm: res/patches/%.pd bin
	webpd -i $< -o $@ -f wasm

$(LIBS): bin
	cp libs/* bin/

bin/index.html: js.html
	cp $< $@

js:$(PATCHES_WASM) $(LIBS) bin/index.html
	haxe js.hxml

js-debug:$(PATCHES_WASM) $(LIBS) bin/index.html
	haxe js.hxml --debug
