const audioContext = new AudioContext()

let patch = null
let stream = null
let webpdNode = null

const initApp = async () => {
	// Register the worklet
	await WebPdRuntime.initialize(audioContext)

	// Fetch the patch code
	response = await fetch('base.wasm')
	patch = await response.arrayBuffer()

	var node = audioContext.createMediaStreamDestination() 
	stream = node.stream
}

const startApp = async () => {
	if (audioContext.state === 'suspended') {
		audioContext.resume()
	}

	// Setup web audio graph
	webpdNode = await WebPdRuntime.run(
		audioContext,
		patch,
		WebPdRuntime.defaultSettingsForRun('./base.wasm'),
	)
	webpdNode.connect(audioContext.destination)

	// Comment this if you don't need audio input
	const sourceNode = audioContext.createMediaStreamSource(stream)
	sourceNode.connect(webpdNode)
}

const labels = {};
const explorePorts = async () => {
	const metadata = await WebPdRuntime.readMetadata(patch)
	Object.entries(metadata.compilation.io.messageReceivers)
		.filter(([_, spec]) => spec.metadata.label !== "" && spec.metadata.label !== undefined)
		.map(([nodeId, spec]) => labels[spec.metadata.label] = nodeId)
}

initApp().then((result) => {return explorePorts(result)})

var Webpd = {
	startApp : startApp,
	sendMsgToWebPd : (l, portletId, message) => {
		webpdNode.port.postMessage({
			type: 'io:messageReceiver',
			payload: {
				nodeId:labels[l],
				portletId,
				message,
			},
		})
	},
}
