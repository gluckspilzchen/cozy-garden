/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023 Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
#if !macro

import dn.M;
import dn.Lib;
import dn.Tweenie;
import dn.data.GetText;
import dn.heaps.slib.*;
import ui.Console;
import tools.*;

#end
