/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023-2024 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
enum Coord { Point (x : Int, y : Int); }
enum Neighbourhood {
	Moore;
	VonNeumann;
}

typedef Constraint = {
	var kind : Int;
	var value : String;
}

class EquatableConstraint {
	var c : Constraint;
	public function new (c : Constraint) {
		this.c = c;
	}

	public function isEqual(ec : EquatableConstraint) {
		return this.c.kind == ec.c.kind && this.c.value == ec.c.value;
	}
}

typedef Constraints = {
	var right : Constraint;
	var left : Constraint;
	var top : Constraint;
	var bottom : Constraint;
	var kind : Constraint;
	var priority : Constraint;
}

typedef JsonTile = {
	var annotations : Constraints;
	var x : Int;
	var y : Int;
}

typedef AllJson = {
	var tileSize : Int;
	var constraints : Array<String>;
	var tiles : Array<JsonTile>;
}

typedef ConstrainedTiles = {
	var tile : h2d.Tile;
	var constraints : Constraints;
}

class Level extends dn.Process {
	public var TILES(default, null) = new Array();
	public var TEMP_TILES(default, null) : Map<Coord, ProtoEntity> = new Map();
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var wid : Int;
	public var hei : Int;

	var tilesetSource : h2d.Tile;
	var normalsSource : h2d.Tile;

	var invalidated = true;

	var tiles : Array<Int>;

	static var tileAnnotations : AllJson = JsonMacro.load("./res/world/tileset_annot.json");
	var baseTiles : Array<ConstrainedTiles>;

	static function getSubTiles(t : h2d.Tile, json : AllJson) : Array<ConstrainedTiles> {
		var texture = t.getTexture();
		var height = texture.height;
		var width = texture.width;
		var size = json.tileSize;

		var tiles = new Array();
		for (j in 0...Math.floor(height/size)) {
			for (i in 0...Math.floor(width/size)) {
				var nt = t.sub(i * size, j * size, size, size);
				
				var cs = json.tiles.filter((t : JsonTile) -> t.x == i && t.y == j);

				if (0 == cs.length) {
					continue;
				}

				tiles.push({ tile : nt, constraints : cs[0].annotations });
			}
		}

		return tiles;
	}

	public var collisions(default, null) : Map<Coord, Bool> = new Map();

	var texts : h2d.Object;
	var fgTexts : h2d.Object;
	var bg : h2d.Object;
	var col : h2d.Object;
	var dyns : h2d.Object;
	var top : h2d.Object;
	var post : h2d.Object;
	var ui : h2d.Object;

	var wfc : tools.WFC<EquatableConstraint>;

	public function new() {
		super(Game.ME);
		createRootInLayers(Game.ME.scroller, Const.DP_BG);

		baseTiles = getSubTiles(hxd.Res.world.tileset.toAseprite().toTiles()[0], tileAnnotations);

		wid = 10;
		hei = 10;

		tiles = new Array();

		texts = new h2d.Object();
		game.scroller.add(texts, Const.DP_BG);

		fgTexts = new h2d.Object();
		game.scroller.add(fgTexts, Const.DP_TOP);

		bg = new h2d.Object(root);
		col = new h2d.Object(root);
		dyns = new h2d.Object(root);
		top = new h2d.Object(root);
		post = new h2d.Object(Boot.ME.postRoot);
		ui = new h2d.Object(Boot.ME.uiRoot);

		var tc = tools.WFC.TileConstraints.fromConstraints(baseTiles.map((t : ConstrainedTiles) -> t.constraints));

		var ts = [for (i in tc.keyValueIterator()) { constraints : i.value, kind : new EquatableConstraint(baseTiles[i.key].constraints.kind) }];

		wfc = new WFC<EquatableConstraint>(wid, hei, ts, new EquatableConstraint({ kind : 1, value : "empty"}));
	}
	
	override function onDispose() {
		super.onDispose();

		texts.remove();
		fgTexts.remove();

		bg.remove();
		col.remove();
		dyns.remove();
		top.remove();
		post.remove();

		for (t in TILES) {
			t.destroy();
		}
		for (t in TEMP_TILES.iterator()) {
			t.destroy();
		}
		TILES = null;
		TEMP_TILES = null;
	}

	static var side = Const.GRID;
	private function getTiles(color : Int, alpha : Float) {
		var w = side;
		var h = side;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(color, w, h, alpha));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(color, w, h, alpha));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function initLevel() {
		invalidated = true;
		tiles = wfc.run();
	}

	public inline function isValidX(cx) return (cx>=0 && cx<wid);
	public inline function isValidY(cy) return (cy>=0 && cy<hei);
	public inline function isValid(cx,cy) return isValidX(cx) && isValidY(cy);
	public inline function coordId(cx,cy) return cx + cy*wid;


	public function render() {
		texts.removeChildren();
		fgTexts.removeChildren();
		bg.removeChildren();
		col.removeChildren();
		dyns.removeChildren();
		post.removeChildren();
		ui.removeChildren();

		var container = new h2d.Object(col);
		container.x = stageWid/2 - wid * side / 2;
		container.y = stageHei/2 - hei * side / 2;

		var ui_container = new h2d.Object(ui);
		ui_container.x = stageWid/2 - wid * side / 2;
		ui_container.y = stageHei/2 - hei * side / 2;

		for (i in 0...wid) {
			for (j in 0...hei) {
				var settings = getTiles(0x000000, 0);
				var b = new ui.Components.ButtonComp(settings, ui_container);

				b.x = i * side;
				b.y = j * side;
				b.onClick = () -> {
					tiles = wfc.setTile(i, j, new EquatableConstraint({ kind : 1, value : "road"}));
					invalidated = true;
					Webpd.sendMsgToWebPd('Tag1', '0', ['bang']);
				};
				b.onRightClick = () -> {
					tiles = wfc.setTile(i, j, new EquatableConstraint({ kind : 1, value : "empty"}));
					invalidated = true;
					Webpd.sendMsgToWebPd('Tag2', '0', ['bang']);
				};

				var b = new h2d.Bitmap(baseTiles[tiles[i + j * wid]].tile, container);
				b.x = i * side;
				b.y = j * side;
			}
		}
	}

	public function hasCollision(cx, cy) : Bool {
		return collisions.exists(Point(cx, cy));
	}

	public function shouldChangeLevel() : Bool {
		return false;
	}

	override function update() {
		super.update();
	}

	override function postUpdate() {
		super.postUpdate();

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function addCollision(cx, cy) {
		collisions.set(Point(cx, cy), true);
	}

	public function removeCollision(cx, cy) {
		collisions.remove(Point(cx, cy));
	}

	public function checkNeighbours(n : Neighbourhood, f : (cx : Int, cy : Int) -> Bool, cx : Int, cy : Int) {
		var res = switch n {
			case Moore: f(cx+1, cy+1) || f(cx-1, cy-1) || f(cx-1, cy+1) || f(cx+1, cy-1);
			case VonNeumann: false;
		};
		return res || f(cx+1, cy) || f(cx-1, cy) || f(cx, cy+1) || f(cx, cy-1);
	}

	override function onResize() {
		super.onResize();

		invalidated = true;
	}
}
