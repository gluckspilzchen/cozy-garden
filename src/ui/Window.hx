/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023-2024 Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

class Window extends dn.Process {
	public var win: h2d.Flow;

	public function new() {
		super(Game.ME);

		createRootInLayers(Game.ME.root, Const.DP_UI);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		win = new h2d.Flow(root);
		win.backgroundTile = h2d.Tile.fromColor(0xffffff, 32,32);
		win.borderWidth = 7;
		win.borderHeight = 7;
		win.layout = Vertical;
		win.verticalSpacing = 2;

		dn.Process.resizeAll();
	}

	public function clearWindow() {
		win.removeChildren();
	}

	public inline function add(e:h2d.Flow) {
		win.addChild(e);
		onResize();
	}

	override function onResize() {
		super.onResize();

		root.scale(Const.UI_SCALE);

		var w = M.ceil( stageWid/Const.UI_SCALE );
		var h = M.ceil( stageHei/Const.UI_SCALE );
		win.x = Std.int( w*0.5 - win.outerWidth*0.5 );
		win.y = Std.int( h*0.5 - win.outerHeight*0.5 );
	}

	function onClose() {}
	public function close() {
		if( !destroyed ) {
			destroy();
			onClose();
		}
	}
}
