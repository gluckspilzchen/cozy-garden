/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023-2024 Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

class Modal extends ui.Window {
	public static var ALL : Array<Modal> = [];
	static var COUNT = 0;

	var ca : dn.legacy.Controller.ControllerAccess;
	var mask : h2d.Bitmap;
	var modalIdx : Int;

	public function new() {
		super();

		ALL.push(this);
		modalIdx = COUNT++;
		if( modalIdx==0 )
			Game.ME.pause();

		ca = Main.ME.controller.createAccess("modal", true);
		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);
		dn.Process.resizeAll();
	}

	public static function hasAny() {
		for(e in ALL)
			if( !e.destroyed )
				return true;
		return false;
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
		ALL.remove(this);
		COUNT--;
		if( !hasAny() )
			Game.ME.resume();
	}

	function closeAllModals() {
		for(e in ALL)
			if( !e.destroyed )
				e.close();
	}

	override function onResize() {
		super.onResize();
		if( mask!=null ) {
			var w = M.ceil( stageWid/Const.UI_SCALE );
			var h = M.ceil( stageHei/Const.UI_SCALE );
			mask.scaleX = w;
			mask.scaleY = h;
		}
	}

	override function postUpdate() {
		super.postUpdate();
		mask.visible = modalIdx==0;
		win.alpha = modalIdx==COUNT-1 ? 1 : 0.6;
	}

	override function update() {
		super.update();
		if( ca.startPressed() )
			closeAllModals();
		if( ca.bPressed() || ca.isKeyboardPressed(hxd.Key.ESCAPE) )
			close();
	}
}
