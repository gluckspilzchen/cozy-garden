/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023-2024 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
import dn.Process;
import hxd.Key;
import tools.CustomControl.Action;
import haxe.ds.Option;

import ldtk.Level.NeighbourDir.*;

typedef Save = {
}

class Game extends Process {
	public static var ME : Game;

	public var ca : dn.legacy.Controller.ControllerAccess;
	public var control : tools.CustomControl;
	public var fx : Fx;
	public var scroller : h2d.Layers;
	public var level : Level;
	public var lights : Lights;
	public var hud : ui.Hud;

	var mouseX : Float;
	var mouseY : Float;

	var target : Option<ldtk.Point>;
	var targetRoot : h2d.Object;

	public var repl : tools.Hake;

	public var colors(default, null) : Array<Int>;
	public var selected(default, null) : Int;

	static var side = Const.GRID;
	private function getTiles(color : Int, alpha : Float) {
		var w = side;
		var h = side;
		var baseTile = new h2d.SpriteBatch(h2d.Tile.fromColor(color, w, h, alpha));
		baseTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		var hoverTile = new Array();
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		hoverTile.push(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		var activeTile = new h2d.SpriteBatch(h2d.Tile.fromColor(color, w, h, alpha));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(color, w, h, alpha)));
		activeTile.add(new h2d.SpriteBatch.BatchElement(h2d.Tile.fromColor(0xeeeeee, w, h, 0.3)));
		return {
			width : w,
			height : h,
			hover : hoverTile,
			active : activeTile,
			base : baseTile,
		};
	}

	public function initColorSelector() {
		colors = [ 0xF0A45D, 0xFA9183, 0xE36BD2 ];
		selected = 0;

		var ui_container = new h2d.Object(Boot.ME.uiRoot);

		var color = 0x000000;
		var g = new h2d.Graphics();
		g.beginFill(color, 0.);
		g.lineStyle(5, color);
		g.drawRect(0, 0, Const.GRID, Const.GRID);
		g.endFill();

		for (i in 0...colors.length) {
			var settings = getTiles(colors[i], 1);
			var button = new ui.Components.ButtonComp(settings, ui_container);
			button.y = i * side;

			button.onClick = () -> {
				selected = i;
				g.y = button.y;
			};

			if (i == 0) {
				g.y = button.y;
			}
		}

		var b = ui_container.getBounds();
		ui_container.x = 0.005 * stageWid;
		ui_container.y = stageHei/2 - b.height/2;

		ui_container.addChild(g);
	}

	public function new() {
		super(Main.ME);
		ME = this;
		ca = Main.ME.controller.createAccess("game");
		ca.setLeftDeadZone(0.2);
		ca.setRightDeadZone(0.2);

		initColorSelector();

		repl = Main.ME.repl;
		repl.addCommand("level", "Opens a menu to explore the level.", logLevel);

		control = new tools.CustomControl(ca);
		var showControls = function () {
			var actions = new Array();
			for (ab in tools.CustomControl.bindings.keyValueIterator()) {
				var bindings = [];
				for (b in ab.value) {
					var s = [ for (m in b.modifiers) tools.CustomControl.getKeyName(m) ];
					s.push(tools.CustomControl.getKeyName(b.key));
					bindings.push(s.join("+"));
				}
				actions.push(tools.CustomControl.getActionName(ab.key) + ": " + bindings.join(" "));
			}
			repl.log(actions.join("\n"));
		};
		repl.addCommand("controls", "List the game controls.", showControls);

		createRootInLayers(Main.ME.root, Const.DP_BG);

		engine.backgroundColor = 0xa2a2a2;

		scroller = new h2d.Layers();
		root.add(scroller, Const.DP_BG);

		target = None;
		targetRoot = new h2d.Object();
		scroller.add(targetRoot, Const.DP_TOP);

		fx = new Fx();
		hud = new ui.Hud();

		startLevel();

		Process.resizeAll();
	}

	public function onCdbReload() {
	}

	private function startLevel() {
		level = new Level();
		lights = new Lights();

		level.initLevel();
	}

	public function previousLevel() {
		fx.clear();
	}
	
	public function updateLevel() {
		save();

		restartLevel();
	}

	public function nextLevel() {
		restartLevel();
	}

	public function restartLevel(){
		fx.clear();
		for(e in Entity.ALL)
			e.destroy();
		level.destroy();
		lights.destroy();
		all_gc();
		startLevel();
	}

	function e_gc() {
		if( vfx.Effect.GC==null || vfx.Effect.GC.length==0 )
			return;

		for(e in vfx.Effect.GC)
			e.dispose();
		vfx.Effect.GC = [];
	}

	function ec_gc() {
		if( ent.comp.EntityComponent.GC==null || ent.comp.EntityComponent.GC.length==0 )
			return;

		for(e in ent.comp.EntityComponent.GC)
			e.dispose();
		ent.comp.EntityComponent.GC = [];
	}


	function gc() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	function s_gc() {
		if( systems.System.GC==null || systems.System.GC.length==0 )
			return;

		for(s in systems.System.GC)
			s.dispose();
		systems.System.GC = [];
	}

	function all_gc() {
		gc();
		ec_gc();
		e_gc();
		s_gc();
	}

	override function onDispose() {
		super.onDispose();

		fx.destroy();
		for(e in Entity.ALL)
			e.destroy();
		all_gc();
	}

	function updateMouse() {
		var gx = hxd.Window.getInstance().mouseX;
		var gy = hxd.Window.getInstance().mouseY;
		mouseX = gx/Const.SCALE - scroller.x;
		mouseY = gy/Const.SCALE - scroller.y;

		hud.invalidate();
	}

	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.preUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.preUpdate();
	}

	function showTarget() {
		targetRoot.removeChildren();

		if (!Settings.assistedAiming)
			return;

		switch(target) {
			case None:
			case Some (t):
				var color = 0xffffff;
				var g = new h2d.Graphics(targetRoot);
				g.beginFill(color, 0.);
				g.lineStyle(2, color);
				g.drawRect(t.cx * Const.GRID, t.cy * Const.GRID, Const.GRID, Const.GRID);
				g.endFill();
		}
	}

	override function postUpdate() {
		super.postUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.postUpdate();
		for(s in systems.System.ALL) if( !s.destroyed ) s.postUpdate();
		all_gc();

		if (level.shouldChangeLevel()) {
			repl.log("Loading level");
			updateLevel();
		}

		showTarget();	

		Boot.ME.postRoot.x = scroller.x;
		Boot.ME.postRoot.y = scroller.y;
	}

	override function fixedUpdate() {
		super.fixedUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}

	override function update() {
		super.update();

		updateMouse();

		for(e in Entity.ALL) if( !e.destroyed ) e.update();
		for(e in ent.comp.EntityComponent.ALL) if( !e.destroyed ) e.update();
		for(e in vfx.Effect.ALL) if( !e.destroyed ) e.update();
		for(s in systems.System.ALL) if( !s.destroyed ) s.update();

		if( !ui.Console.ME.isActive() && !ui.Modal.hasAny() ) {
			if( control.probe(Exit) ) {
				new PauseMenu();
			}

			if(ca.isKeyboardPressed(Key.B)) {
				previousLevel();
			}

			if(ca.isKeyboardPressed(Key.N)) {
				nextLevel();
			}

			if(control.probe(AimUp) || control.probe(AimDown) || control.probe(AimLeft) || control.probe(AimRight)) {
				updateTarget(Std.int(ca.lxValue()), Std.int(ca.lyValue()));
			}

			if(control.probe(SelectAim)) {
				logTarget();
			}
		}
	}

	public function updateTarget(x : Int, y : Int) {
		target =
			switch (target) {
				case Some (t) :
					var ncx = t.cx + x;
					var ncy = t.cy + y;

					if (level.isValidX(ncx))
						t.cx = ncx;

					if (level.isValidY(ncy))
						t.cy = ncy;

					if (Settings.assistedAiming) {
						repl.log("Cursor at [" + t.cx + "," + t.cy + "]");

						var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

						for (e in entities) {
							repl.log(e.getLog());
						}
					}
					Some (t);
				case None: Some (new ldtk.Point(0, 0));
			}
	}

	public function getTarget() {
		return
			switch (target) {
				case Some (t) if (Settings.assistedAiming):
					new h2d.col.Point((t.cx + 0.5) * Const.GRID, (t.cy + 0.5) * Const.GRID);
				default: new h2d.col.Point(mouseX, mouseY);
			}
	}

	public function logTarget() {
		switch (target) {
			case Some (t):
				var entities = ProtoEntity.ALL.filter(function (e) {return e.cx == t.cx && e.cy == t.cy;});

				var menus = new Array();

				var back = {
					label : "Back",
					select : function () {},
				};

				menus.push(back);

				for (e in entities) {
					var o = {
						label : e.getName(),
						select : e.details,
					}

					menus.push(o);
				}

				repl.menu(menus);
			default:
		}
	}

	public function logLevel() {
		var entities = new Map();

		for (e in ProtoEntity.ALL) {
			var es = entities.get(e.getName());

			if (es == null)
				es = new Array();

			es.push(e);

			entities.set(e.getName(), es);
		}

		var menus = new Array();

		for (i in entities.keyValueIterator()) {
			var locations = [ for (e in i.value) "[" + e.cx + ", " + e.cy + "]" ];
			var o = {
				label : i.key,
				select : function () {
					repl.log(locations.join(" "));
				},
			}

			menus.push(o);
		}

		var exits = new Array();

		for (i in 0...level.wid) {
			if (!level.hasCollision(i, 0)) {
				exits.push(new ldtk.Point(i, 0));
			}

			if (!level.hasCollision(i, level.hei - 1)) {
				exits.push(new ldtk.Point(i, level.hei - 1));
			}
		}

		for (i in 0...level.hei) {
			if (!level.hasCollision(0, i)) {
				exits.push(new ldtk.Point(0, i));
			}

			if (!level.hasCollision(level.wid - 1, i)) {
				exits.push(new ldtk.Point(level.wid - 1, i));
			}
		}

		var exitLocations = [for (e in exits) "[" + e.cx + ", " + e.cy + "]"];
		var o = {
			label : "Exits",
			select : function () {
				repl.log(exitLocations.join(" "));
			},
		}
		menus.push(o);

		repl.menu(menus);
	}

	private function getCurrentState() {
		var s = {
		}

		return s;
	}

	public function save() {
		var s = getCurrentState();

		hxd.Save.save(s);
	}	

	public function load() {
		var cs = getCurrentState();
		var s : Save = hxd.Save.load(cs);

		restartLevel();
	}
}
