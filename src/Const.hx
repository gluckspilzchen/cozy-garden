/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023 Quentin Lambert, Sebastien Benard

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

class Const {
	public static var FPS = 60;
	public static var FIXED_FPS = 30;
	public static var AUTO_SCALE_TARGET_WID = -1; // -1 to disable auto-scaling on width
	public static var AUTO_SCALE_TARGET_HEI = -1; // -1 to disable auto-scaling on height

	public static var LEVEL_X = 24;
	public static var LEVEL_Y = 11;

	/** Viewport scaling **/
	public static var SCALE(get,never) : Int;
	static inline function get_SCALE() {
		return 1;
	}

	/** Specific scaling for top UI elements **/
	public static var UI_SCALE(get,never) : Float;
	static inline function get_UI_SCALE() {
		// can be replaced with another way to determine the UI scaling
		return 1;
	}

	public static var GRID = 32;

	static var _uniq = 0;
	public static var NEXT_UNIQ(get,never) : Int; static inline function get_NEXT_UNIQ() return _uniq++;
	public static var INFINITE = 999999;

	static var _inc = 0;
	public static var DP_BG = _inc++;
	public static var DP_FX_BG = _inc++;
	public static var DP_MAIN = _inc++;
	public static var DP_FRONT = _inc++;
	public static var DP_FX_FRONT = _inc++;
	public static var DP_TOP = _inc++;
	public static var DP_UI = _inc++;

	public static var SPELL_OFFSET = 1;
	public static var GRAVITY = .0001;
}
