/*
    Cozy Garden, a small experimental game inspired by townscaper.
    Copyright (C) 2021-2023 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

macro function load(path : String) {
	return try {
		var json = haxe.Json.parse(sys.io.File.getContent(path));
		macro $v{json};
	} catch (e) {
		haxe.macro.Context.error('Failed to load json: $e', haxe.macro.Context.currentPos());
	}
}
