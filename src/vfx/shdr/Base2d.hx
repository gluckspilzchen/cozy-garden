/*
    CozyGarden, a relaxed experiment in procedural technology.
    Copyright (C) 2023 Quentin Lambert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Init extends hxsl.Shader {

	static var SRC = {
		var pixelNormal : Vec4;

		function __init__() {
			pixelNormal = vec4(0, 0, 1, 0);
		}
	};
}

class Output extends hxsl.Shader {

	static var SRC = {

		var output : {
			var position : Vec4;
			var color : Vec4;
			var normal : Vec4;
		}

		var pixelNormal : Vec4;

		function fragment() {
			output.normal = pixelNormal;
		}
	};
}
